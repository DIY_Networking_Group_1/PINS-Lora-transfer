#include "Utils.h"

void checkHeap(int pos) {
    if (heap_caps_check_integrity_all(true)) {
        log_info("HEAP check passed at: " + std::to_string(pos));
    } else {
        log_error("HEAP check failed at: " + std::to_string(pos));
    }
}

char getRandomByte() {
    return static_cast<char>(esp_random());
}

float getRandomFloat() {
    return static_cast<float>(esp_random()) / static_cast<float>(UINT32_MAX);
}

uint32_t getRandomUInt32() {
    return esp_random();
}
