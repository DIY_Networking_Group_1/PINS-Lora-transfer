#pragma once

#include <mutex>
#include <condition_variable>
#include <vector>
#include "messages/serial/SerialMessage.h"

// Based on: https://juanchopanzacpp.wordpress.com/2013/02/26/concurrent-queue-c11/
template <typename T>
class Queue {
 public:
    Queue() {
        queueMutex = new std::mutex();
        condVar = new std::condition_variable();
    }

    ~Queue() {
        delete queueMutex;
        delete condVar;
    }

    T pop() {
        std::unique_lock<std::mutex> mlock(*queueMutex);
        while (queueVec.empty()) {
            condVar->wait(mlock);
        }
        auto item = queueVec.front();
        queueVec.erase(queueVec.begin());
        return item;
    }

    void pop(T &item) {
        std::unique_lock<std::mutex> mlock(*queueMutex);
        while (queueVec.empty()) {
            condVar->wait(mlock);
        }
        item = queueVec.front();
        queueVec.erase(queueVec.begin());
    }

    void push(T &&item) {
        std::unique_lock<std::mutex> mlock(*queueMutex);
        queueVec.push_back(std::move(item));
        mlock.unlock();
        condVar->notify_one();
    }

    void push(T &item) {
        std::unique_lock<std::mutex> mlock(*queueMutex);
        queueVec.push_back(item);
        mlock.unlock();
        condVar->notify_one();
    }

    void clear() {
        std::unique_lock<std::mutex> mlock(*queueMutex);
        queueVec.clear();
        mlock.unlock();
        condVar->notify_one();
    }

    int size() {
        return queueVec.size();
    }

 protected:
    std::vector<T> queueVec;
    std::mutex* queueMutex;
    std::condition_variable* condVar;
};
