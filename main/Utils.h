#pragma once

#include "esp_heap_caps.h"
#include "esp_system.h"
#include "Logger.h"
#include "messages/MessageTools.h"

/**
 * To use it:
 * make menuconfig 
 * "Component config" --> "Heap memory debugging"
 * --> "Heap corruption detection" --> "Light impact"
 */
void checkHeap(int pos);
/**
 * Returns a random float between 0 and 1.
 */
float getRandomFloat();
/**
 * Returns a random byte.
 */
char getRandomByte();
/**
 * Returns a random unsigned 32 bit int.
 */
uint32_t getRandomUInt32();
