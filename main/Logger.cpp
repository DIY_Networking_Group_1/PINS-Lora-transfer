#include "Logger.h"

void log_debug(std::string msg) {
    if (LOG_LEVEL >= l_debug) {
        std::cout << "[LORA_32][DEBUG]: " << msg << std::endl;
    }
}

void log_info(std::string msg) {
    if (LOG_LEVEL >= l_info) {
        std::cout << "[LORA_32][INFO]: " << msg << std::endl;
    }
}

void log_warn(std::string msg) {
    if (LOG_LEVEL >= l_warn) {
        std::cout << "[LORA_32][WARN]: " << msg << std::endl;
    }
}

void log_error(std::string msg) {
    if (LOG_LEVEL >= l_error) {
        std::cerr << "[LORA_32][ERROR]: " << msg << std::endl;
    }
}
