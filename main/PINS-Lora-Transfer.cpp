#include "PINS-Lora-Transfer.h"

void app_main() {
    loraTransfer.app_main();
}

LoraTransfer::LoraTransfer() {}

LoraTransfer::~LoraTransfer() {
    delete serialC;
    delete loraC;
}

void LoraTransfer::app_main() {
    // Setup GPIOs:
    EspGpioHelper::init();
    vTaskDelay(100);

    // Serial:
    serialC = new SerialConnection();
    serialC->init();
    startSerialConnection();

    // Lora:
    EspGpioHelper::setLoraModeOutGpio(1);
    if (EspGpioHelper::getLoraModeInGpioLevel()) {
        loraC = new LoraRepeaterConnection(serialC);
        log_info("Started in repeater mode.");
    } else {
        loraC = new LoraRaspberryConnection(serialC);
        log_info("Started in raspberry mode.");
    }
    EspGpioHelper::setLoraModeOutGpio(0);
    loraC->init();
    startLoraConnection();
}

void LoraTransfer::startLoraConnection() {
    xTaskCreatePinnedToCore(&loraConnectionTask, "loraConnectionTask", 4096, loraC, 5, NULL, 0);
}

void LoraTransfer::startSerialConnection() {
    xTaskCreatePinnedToCore(&serialConnectionTask, "serialConnectionTask", 4096, serialC, 5, NULL, 1);
}

void LoraTransfer::loraConnectionTask(void* pvParameter) {
    log_info("Lora connection running on core: " + std::to_string(xPortGetCoreID()));
    xPortGetCoreID();
    LoraRaspberryConnection* loraC = reinterpret_cast<LoraRaspberryConnection*>(pvParameter);
    loraC->run();
}

void LoraTransfer::serialConnectionTask(void* pvParameter) {
    log_info("Serial connection running on core: " + std::to_string(xPortGetCoreID()));
    xPortGetCoreID();
    SerialConnection* serialC = reinterpret_cast<SerialConnection*>(pvParameter);
    serialC->run();
}
