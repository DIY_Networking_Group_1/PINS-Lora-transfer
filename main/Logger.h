#pragma once

#include <iostream>
#include <string>

enum LogLevel {
    l_none = 0,
    l_error = 1,
    l_warn = 2,
    l_info = 3,
    l_debug = 4
};

#define LOG_LEVEL l_none

void log_debug(std::string msg);
void log_info(std::string msg);
void log_warn(std::string msg);
void log_error(std::string msg);
