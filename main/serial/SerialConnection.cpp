#include "SerialConnection.h"
#include "lora/AbstractLoraConnection.h"

SerialConnection::SerialConnection() {
}

void SerialConnection::init() {
    if (state != sc_init) {
        log_error(LOGGER_PREFIX + "Unable to init. State != sc_init");
        return;
    }

    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
    };
    // Configure UART_NUM_0 parameters
    uart_param_config(UART_NUM_0, &uart_config);

    // Set UART_NUM_0 pins(TX: GPIO_NUM_1, RX: GPIO_NUM_3)
    uart_set_pin(UART_NUM_0, GPIO_NUM_1, GPIO_NUM_3, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    // Install UART driver (we don't need an event queue here)
    // In this example we don't even use a buffer for sending data.
    uart_driver_install(UART_NUM_0, SERIAL_RECEIVE_BUFFER_SIZE, 0, 0, NULL, 0);

    state = sc_ready;
    log_info(LOGGER_PREFIX + "Finished init.");
}

int SerialConnection::sr_write(uchar* buf, int length) {
    if (state != sc_ready) {
        log_error(LOGGER_PREFIX + "Unable to write. State != sc_ready");
        return -1;
    }
    uart_write_bytes(UART_NUM_0, (const char*)buf, length);
    uart_write_bytes(UART_NUM_0, "\n", 1);
    // printf("%s\n", std::string(reinterpret_cast<char*>(buf), length).c_str());
    // std::cout << std::string(reinterpret_cast<char*>(buf), length) << std::endl;
    return 0;
}

ssize_t SerialConnection::sr_read(uchar* buf, size_t length) {
    if (state != sc_ready) {
        log_error(LOGGER_PREFIX + "Unable to write. State != sc_ready");
        return -1;
    }

    uchar c = 0xFF;
    int count = 0;

    int i = 0;
    while (i < length) {
        count = uart_read_bytes(UART_NUM_0, &c, 1, 100);

        if (count > 0) {
            if (c == '\n' || c == 0xFF) {
                return i;
            }
            buf[i++] = c;
        }
        trySendBufReady();
    }
    return i;
}

void SerialConnection::trySendBufReady() {
    if (!bufFullSend && nextBufReadyMsgSendTime < esp_timer_get_time()) {
        nextBufReadyMsgSendTime = esp_timer_get_time() + SERIAL_SEND_BUFFER_READY_INTERVALL_MICRO;
        sendBufReady();
    }
}

void SerialConnection::sendBufFull() {
    SerialMsg msg = SerialMsg(t_send_buffer_full);
    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);
    sr_write(msgSer, msg.getMsgLength());
    delete[] msgSer;
    bufFullSend = true;
    log_debug(LOGGER_PREFIX + "Send Lora buffer full over serial.");
}

void SerialConnection::sendBufReady() {
    SerialMsg msg = SerialMsg(t_send_buffer_ready);
    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);
    sr_write(msgSer, msg.getMsgLength());
    delete[] msgSer;
    bufFullSend = false;
    log_debug(LOGGER_PREFIX + "Send Lora buffer ready over serial.");
}

void SerialConnection::sendLoraReceived(std::string s) {
    SerialMsg msg = SerialMsg(t_received_Lora_data, reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), s.size());
    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);
    sr_write(msgSer, msg.getMsgLength());
    delete[] msgSer;
    log_debug(LOGGER_PREFIX + "Send Lora received over serial.");
}

bool SerialConnection::hasSendBufFull() {
    return bufFullSend;
}

void SerialConnection::setLoraConnection(AbstractLoraConnection* loraC) {
    this->loraC = loraC;
}

void SerialConnection::run() {
    if (state != sc_ready) {
        log_error(LOGGER_PREFIX + "Unable to run. State != sc_ready");
        return;
    }

    uchar buf[SERIAL_RECEIVE_BUFFER_SIZE];
    ssize_t l = 0;
    while (1) {
        trySendBufReady();

        l = sr_read(buf, SERIAL_RECEIVE_BUFFER_SIZE);
        if (l > 0) {
            log_debug(LOGGER_PREFIX + "Received " + std::to_string(l) + " bytes over serial: " + std::string(reinterpret_cast<char*>(buf), l));
            SerialMsg msg = SerialMsg(buf, l);
            if (msg.getState() == i_success) {
                switch (msg.getType()) {
                    case t_send_Lora_data:
                    {
                        if (msg.getPayloadLength() > 0) {
                            log_debug(LOGGER_PREFIX + "Received send Lora over serial with payloadLength: " + std::to_string(msg.getPayloadLength()));
                            uchar_sp payload = makeSharedUCharArrayPtr(msg.getPayload(), msg.getPayloadLength());
                            uchar_sp addr = makeSharedUCharArrayPtr(msg.getTMPDST(), 6);
                            loraC->send(addr, payload, msg.getPayloadLength());
                        } else {
                            log_warn("Received send lora over serial with payloadLength <= 0!");
                            printByteArray(buf, l);
                        }
                    }
                        break;

                    case t_update_display:
                        log_debug(LOGGER_PREFIX + "Received update display over serial.");
                        break;

                    case t_set_Lora_address:
                    {
                        uchar_sp localMac = makeSharedUCharArrayPtr(msg.getTMPDST(), 6);
                        loraC->setLocalMac(localMac);
                    }
                        break;

                    default:
                        log_warn(LOGGER_PREFIX + "Unknown serial message received: " + std::to_string(msg.getType()));
                        break;
                }
            } else {
                log_warn(LOGGER_PREFIX + "Failed to parse serial message.");
            }
        } else {
            log_debug(LOGGER_PREFIX + "Serial read nothing read.");
        }
    }
}
