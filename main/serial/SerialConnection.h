#pragma once

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/uart.h"
#include "messages/serial/SerialMessage.h"
#include "Utils.h"

#define SERIAL_RECEIVE_BUFFER_SIZE 1024
#define SERIAL_SEND_BUFFER_READY_INTERVALL_MICRO 10 * 1000 * 1000

class AbstractLoraConnection;

enum SerialConnectionState {
    sc_init = 0,
    sc_ready = 1
};

class SerialConnection {
 public:
    SerialConnection();
    void init();
    void sendBufFull();
    void sendBufReady();
    void sendLoraReceived(std::string s);
    bool hasSendBufFull();
    void setLoraConnection(AbstractLoraConnection* loraC);
    void run();


 private:
    const std::string LOGGER_PREFIX = "[Serial] ";

    SerialConnectionState state = sc_init;
    bool bufFullSend = false;
    AbstractLoraConnection* loraC = NULL;
    int64_t nextBufReadyMsgSendTime = INT64_MIN;

    ssize_t sr_read(uchar* buf, size_t length);
    int sr_write(uchar* buf, int length);
    void trySendBufReady();
};
