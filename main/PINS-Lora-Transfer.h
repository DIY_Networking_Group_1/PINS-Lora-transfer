#pragma once

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "serial/SerialConnection.h"
#include "lora/AbstractLoraConnection.h"
#include "lora/LoraRaspberryConnection.h"
#include "lora/LoraRepeaterConnection.h"
#include "EspGpioHelper.h"

#if defined (__cplusplus)
extern "C" {
#endif

void app_main(void);

#if defined (__cplusplus)
}
#endif

class LoraTransfer {
 public:
    LoraTransfer();
    ~LoraTransfer();
    void app_main();

 private:
    AbstractLoraConnection* loraC = NULL;
    SerialConnection* serialC = NULL;

    void startLoraConnection();
    void startSerialConnection();
    static void loraConnectionTask(void* pvParameter);
    static void serialConnectionTask(void* pvParameter);
};

static LoraTransfer loraTransfer;
