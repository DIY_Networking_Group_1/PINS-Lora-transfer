#include "LoraMessageCache.h"

LoraMessageCache::LoraMessageCache() {
    this->map = MsgPartMap();
}

LoraMessageCache::~LoraMessageCache() {
    for (MsgPartMap::iterator iter = map.begin(); iter != map.end(); iter++) {
        clearBurst(iter->second, 0);
    }
}

bool LoraMessageCache::onMessage(uchar_sp senderMac, uchar_sp payload, size_t payloadLength, bool firstPart, bool lastPart, uchar burstNumber, uchar seqNumber) {
    std::string mac = getMacAsString(senderMac);
    MsgPartMap::iterator iter = map.find(mac);
    if (iter != map.end()) {
        // Check if message is for the same burst:
        if (iter->second.get()->burstNumber != burstNumber) {
            if (firstPart || iter->second.get()->allPartsReceived) {
                // New burst with first message received from sender:
                clearBurst(iter->second, burstNumber);
            } else {
                // Burst number does not match and message is not first message of a burst:
                log_warn("Received message burst number with not matching burst number. " + std::to_string(iter->second.get()->burstNumber) + " != " + std::to_string(burstNumber));
                return false;
            }
        }
        insert(iter->second, payload, payloadLength, firstPart, lastPart, seqNumber);
    } else {
        ReceivedMsgBurst_sp burst = std::make_shared<ReceivedMsgBurst>(ReceivedMsgBurst());
        burst.get()->burstNumber = burstNumber;
        burst.get()->allPartsReceived = false;
        burst.get()->changedSinceLastCheck = true;
        burst.get()->senderMac = senderMac;
        insert(burst, payload, payloadLength, firstPart, lastPart, seqNumber);
        map[mac] = burst;
    }
    return true;
}

void LoraMessageCache::clearBurst(ReceivedMsgBurst_sp burst, uchar newBurstNumber) {
    burst->burstNumber = newBurstNumber;
    burst->allPartsReceived = false;
    burst->changedSinceLastCheck = true;
    burst->parts.clear();
}

void LoraMessageCache::insert(ReceivedMsgBurst_sp burst, uchar_sp payload, size_t payloadLength, bool firstPart, bool lastPart, uchar seqNumber) {
    burst->changedSinceLastCheck = true;
    // Generate all parts until the current sequence number:
    for (int i = burst->parts.size(); i <= seqNumber; i++) {
        MsgPart genPart;
        genPart.acked = false;
        genPart.available = false;
        genPart.seqNumber = i;
        genPart.payload = NULL;
        genPart.payloadLength = 0;
        genPart.lastPart = false;
        burst->parts.push_back(genPart);
    }

    // Insert/replace the actual data:
    MsgPart* part = &(burst->parts[seqNumber]);

    part->payload = payload;

    // Update misc properties:
    part->acked = false;
    part->available = true;
    part->lastPart = lastPart;
    part->payloadLength = payloadLength;
}

std::string LoraMessageCache::getMacAsString(uchar_sp mac) {
    return std::string(reinterpret_cast<char*>(mac.get()), 6);
}

bool LoraMessageCache::hasAllPartsReceived(ReceivedMsgBurst_sp burst) {
    for (int i = 0; i < burst->parts.size(); i++) {
        if (!burst->parts[i].available) {
            return false;
        }
    }
    return burst->parts.size() > 0 && burst->parts.end()->lastPart;
}

uchar_sp LoraMessageCache::getToAckParts(ReceivedMsgBurst_sp burst, size_t* ackCount) {
    uchar_sp ackNumbers = makeSharedUCharArrayPtr(LORA_MAX_ACK_NUMBERS_COUNT);
    for (int i = 0; i < burst->parts.size(); i++) {
        if (!burst->parts[i].acked) {
            ackNumbers.get()[(*ackCount)++] = i;
        }
    }
    return ackNumbers;
}

void LoraMessageCache::checkAll(CheckResult* results) {
    for (MsgPartMap::iterator iter = map.begin(); iter != map.end(); iter++) {
        if (iter->second.get()->changedSinceLastCheck) {
            iter->second.get()->changedSinceLastCheck = false;
            results->changed.push_back(iter->second);

            // Check if all parts got received:
            if (!iter->second.get()->allPartsReceived && hasAllPartsReceived(iter->second)) {
                iter->second.get()->allPartsReceived = true;
                iter->second.get()->result = getResultString(iter->second);
                results->messages.push_back(iter->second.get()->result);
            }
        }
    }
}

std::string LoraMessageCache::getResultString(ReceivedMsgBurst_sp burst) {
    std::vector<char> vec;
    for (int i = 0; i < burst->parts.size(); i++) {
        for (int e = 0; e < burst->parts[i].payloadLength; e++) {
            vec.push_back(burst->parts[i].payload.get()[e]);
        }
    }
    vec.push_back('\0');
    return std::string(&(vec[0]), vec.size());
}
