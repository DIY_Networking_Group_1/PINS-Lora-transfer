#pragma once

#include <unordered_map>
#include <vector>
#include <string>
#include <cstring>
#include <memory>
#include "messages/AbstractLoraMessage.h"
#include "messages/LoraDataMessage.h"
#include "messages/LoraAckMessage.h"
#include "Logger.h"
#include "Utils.h"

struct MsgPart {
    uchar seqNumber;
    uchar_sp payload;
    size_t payloadLength;
    bool acked;
    bool available;
    bool lastPart;
};

struct ReceivedMsgBurst {
    uchar burstNumber;
    uchar_sp senderMac;
    std::vector<MsgPart> parts;
    std::string result;
    bool allPartsReceived;
    bool changedSinceLastCheck;
};

typedef std::shared_ptr<ReceivedMsgBurst> ReceivedMsgBurst_sp;

struct CheckResult {
    std::vector<std::string> messages;
    std::vector<ReceivedMsgBurst_sp> changed;
};

typedef std::unordered_map<std::string, ReceivedMsgBurst_sp> MsgPartMap;

class LoraMessageCache {
 public:
    LoraMessageCache();
    ~LoraMessageCache();
    bool onMessage(uchar_sp senderMac, uchar_sp payload, size_t payloadLength, bool firstPart, bool lastPart, uchar burstNumber, uchar seqNumber);
    uchar_sp getToAckParts(ReceivedMsgBurst_sp burst, size_t* ackCount);
    /**
     * Checks all bursts and if all parts got received assembles the actual message.
     * Returns all messages that got assembled.
     */
    void checkAll(CheckResult* results);

 private:
    MsgPartMap map;
    void insert(ReceivedMsgBurst_sp burst, uchar_sp payload, size_t payloadLength, bool firstPart, bool lastPart, uchar seqNumber);
    void clearBurst(ReceivedMsgBurst_sp burst, uchar newBurstNumber);
    bool hasAllPartsReceived(ReceivedMsgBurst_sp burst);
    std::string getMacAsString(uchar_sp mac);
    std::string getResultString(ReceivedMsgBurst_sp burst);
};
