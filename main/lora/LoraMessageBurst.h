#pragma once

#include <unordered_map>
#include <mutex>
#include <memory>
#include "messages/LoraAckMessage.h"
#include "messages/LoraDataMessage.h"
#include "Utils.h"

#define MAX_MSG_BURST_SIZE LORA_MAX_ACK_NUMBERS_COUNT

class LoraRaspberryConnection;

typedef std::unordered_map<uchar, std::shared_ptr<Message>> MsgBurstMap;

class LoraMessageBurst {
 public:
    explicit LoraMessageBurst(uchar burstNumber);
    ~LoraMessageBurst();
    void add(std::shared_ptr<Message> msg, uchar seqNumber);
    int size();
    void onAckMessage(uchar_sp buf);
    uchar getNextSeqNumber();
    uchar getBurstNumber();
    unsigned int getResendCount();
    void setResendCount(unsigned int resendCount);
    unsigned int getBurstRetryCount();
    void setBurstRetryCount(unsigned int burstRetryCount);
    MsgBurstMap* getMap();

 private:
    uchar burstNumber = 0;
    unsigned int resendCount = 0;
    unsigned int burstRetryCount = 0;
    MsgBurstMap map;
    std::mutex* mapMutex = new std::mutex();
    std::mutex* nextSeqNumberMutex = new std::mutex();
    uchar nextSeqNumber = 0;
};
