#include "AbstractLoraConnection.h"

AbstractLoraConnection::AbstractLoraConnection(SerialConnection* serialC) {
    this->serialC = serialC;
    this->serialC->setLoraConnection(this);
}

void AbstractLoraConnection::init() {
    // Load chip MAC:
    loadChipMac();

    // General Lora init:
    lora_init();
    lora_set_frequency(LORA_FREQ_866E6);
    lora_set_bandwidth(LORA_BANDWITH);
    lora_set_tx_power(LORA_TX_POWER);
    lora_enable_crc();
}

uchar_sp AbstractLoraConnection::getChipMac() {
    return chipMac;
}

void AbstractLoraConnection::printMac(uchar_sp mac) {
    printf("%02X:%02X:%02X:%02X:%02X:%02X\n", mac.get()[0], mac.get()[1], mac.get()[2], mac.get()[3], mac.get()[4], mac.get()[5]);
}

void AbstractLoraConnection::printChipMac() {
    printMac(chipMac);
}

void AbstractLoraConnection::loadChipMac() {
    chipMac = makeSharedUCharArrayPtr(6);
    esp_err_t result = esp_efuse_mac_get_default(chipMac.get());
    switch (result) {
        case ESP_OK:
            log_info(LOGGER_PREFIX + "Successfully loaded chip MAC:");
            printMac(chipMac);
            break;
        case ESP_FAIL:
            log_error(LOGGER_PREFIX + "Failed to load chip MAC!");
            break;
    }
}

void AbstractLoraConnection::setLocalMac(uchar_sp localMac) {
    this->localMac = localMac;
}

uchar_sp AbstractLoraConnection::getLocalMac() {
    if (localMac) {
        return localMac;
    }
    return chipMac;
}

void AbstractLoraConnection::receiveBurst() {
    lora_receive();
    int l;
    int receiveCount = 0;
    uchar_sp readBuf;
    int64_t endRead = esp_timer_get_time() + LORA_MIN_READ_TIME_MICRO;
    int64_t endLastRead = INT64_MIN;
    float rssi = 0;
    float snr = 0;
    while (receiveCount < 64 && (endRead > esp_timer_get_time() && endLastRead <= esp_timer_get_time())) {
        l = 0;
        readBuf = makeSharedUCharArrayPtr(LORA_RECEIVE_BUFFER_SIZE);
        if (lora_received()) {
            l = lora_receive_packet(readBuf.get(), LORA_RECEIVE_BUFFER_SIZE);
            if (l > 0) {
                endLastRead = esp_timer_get_time() + LORA_MIN_LAST_READ_DELAY_MICRO;
                receiveCount++;
                rssi = lora_packet_rssi();
                snr = lora_packet_snr();
                log_debug(LOGGER_PREFIX + "Received message with length: " + std::to_string(l) + " RSSI: " + std::to_string(rssi) + " SNR: " + std::to_string(snr));
                onLoraMessage(readBuf, l);
            }
        }
        vTaskDelay(1);
    }
}

bool AbstractLoraConnection::compMac(uchar_sp a, uchar_sp b) {
    for (int i = 0; i < 6; i++) {
        if (a.get()[i] != b.get()[i]) {
            return false;
        }
    }
    return true;
}

bool AbstractLoraConnection::isTarget(uchar_sp localMac, uchar_sp receiverMac) {
    if (compMac(receiverMac, (uchar_sp)WILDCARD_MAC)) {
        return true;
    }
    return compMac(receiverMac, localMac);
}

void AbstractLoraConnection::loraSend(uchar_sp buf, int size) {
    if (size <= 0) {
        log_warn(LOGGER_PREFIX + "Unable to send buf with size <= 0!");
        return;
    }

    log_debug(LOGGER_PREFIX + "Sending Lora message with length: " + std::to_string(size));

    EspGpioHelper::setLoraSendGpio(1);
    lora_send_packet(reinterpret_cast<uint8_t*>(buf.get()), size);
    // Delay to ensure message got send:
    vTaskDelay(5);
    EspGpioHelper::setLoraSendGpio(0);

    log_debug(LOGGER_PREFIX + "Message send.");
}
