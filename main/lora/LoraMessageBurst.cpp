#include "LoraMessageBurst.h"

LoraMessageBurst::LoraMessageBurst(uchar burstNumber) {
    this->burstNumber = burstNumber;
}

LoraMessageBurst::~LoraMessageBurst() {
    // delete mapMutex;
    // delete nextSeqNumberMutex;
}

void LoraMessageBurst::add(std::shared_ptr<Message> msg, uchar seqNumber) {
    std::unique_lock<std::mutex> mlock(*mapMutex);
    map[seqNumber] = msg;
    mlock.unlock();
}

int LoraMessageBurst::size() {
    std::unique_lock<std::mutex> mlock(*mapMutex);
    int i  = map.size();
    mlock.unlock();
    return i;
}

unsigned int LoraMessageBurst::getResendCount() {
    return resendCount;
}

void LoraMessageBurst::setResendCount(unsigned int resendCount) {
    this->resendCount = resendCount;
}

unsigned int LoraMessageBurst::getBurstRetryCount() {
    return burstRetryCount;
}

void LoraMessageBurst::setBurstRetryCount(unsigned int burstRetryCount) {
    this->burstRetryCount = burstRetryCount;
}

void LoraMessageBurst::onAckMessage(uchar_sp buf) {
    uchar flags = LoraAckMessage::getFlagsFromMessage(buf);

    if (flags & LORA_ALL_RECEIVED_FLAG) {
        std::unique_lock<std::mutex> mlock(*mapMutex);
        map.clear();
        mlock.unlock();
    } else {
        uchar ackCount = LoraAckMessage::getAckCountFromMessage(buf);
        uchar_sp ackNumbers = LoraAckMessage::getAckNumbersFromMessage(buf, ackCount);

        std::unique_lock<std::mutex> mlock(*mapMutex);
        for (int i = 0; i < ackCount; i++) {
            // delete map[ackNumbers[i]]->buf;
            map.erase(ackNumbers.get()[i]);
        }
        mlock.unlock();
    }
}

uchar LoraMessageBurst::getNextSeqNumber() {
    std::unique_lock<std::mutex> mlock(*nextSeqNumberMutex);
    // Only 4 bit are available for the msg sequence number:
    uchar c;
    if (nextSeqNumber >= 16) {
        nextSeqNumber = 0;
        c = 0;
    } else {
        c = nextSeqNumber++;
    }
    mlock.unlock();

    return c;
}

uchar LoraMessageBurst::getBurstNumber() {
    return burstNumber;
}

MsgBurstMap* LoraMessageBurst::getMap() {
    return &map;
}
