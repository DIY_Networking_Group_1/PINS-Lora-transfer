#pragma once

#include <string>
#include <atomic>
#include <memory>
#include "Logger.h"
#include "Queue.h"
#include "LoraMessageCache.h"
#include "LoraMessageBurst.h"
#include "messages/LoraDataMessage.h"
#include "messages/LoraAckMessage.h"
#include "Utils.h"
#include "AbstractLoraConnection.h"

// 1 second in microseconds
#define LORA_ACK_TIMER_DELAY_MICRO 1 * 1000 * 1000

#define LORA_MAX_SEND_QUEUE 10
// 1 second in microseconds
#define LORA_MAX_BACKOFF_TIME_MICRO 1 * 1000 * 1000

#define LORA_MAX_BURST_RETRY_COUNT 5
#define LORA_MAX_RESEND_COUNT 10

enum LoraRaspberryConnectionState {
    lra_init,
    lra_ready,
    lra_awaitAck,
};

class LoraRaspberryConnection : public AbstractLoraConnection {
 public:
    explicit LoraRaspberryConnection(SerialConnection* serialC);
    ~LoraRaspberryConnection();
    void init() override;
    void run() override;
    void send(uchar_sp receiverMac, uchar_sp buf, int size) override;
    void send(uchar_sp buf, int size) override;

 private:

    std::atomic<LoraRaspberryConnectionState> state{lra_init};
    Queue<std::shared_ptr<LoraMessageBurst>>* toSendBurstQueue = new Queue<std::shared_ptr<LoraMessageBurst>>();
    std::mutex* curBurstMutex = new std::mutex();
    std::shared_ptr<LoraMessageBurst> curBurst;
    uchar nextBurstNumber = 0;
    std::mutex* burstNumberMutex = new std::mutex();
    Queue<std::shared_ptr<Message>>* toSendAckQueue = new Queue<std::shared_ptr<Message>>();
    int64_t nextAckTimerAlarm = INT64_MAX;
    LoraMessageCache msgCache;

    uchar getNextBurstNumber();
    bool sendOutstandingAckMessages();
    void sendOutstandingDataMessages();
    void sendLoraMessageBurst(LoraMessageBurst* burst);
    void resetTimer();
    bool hasAckTimerAlarmTriggered();
    void fillQueue();
    void onLoraMessage(uchar_sp buf, int l) override;
    void messageBurstSendAll(MsgBurstMap* map);
    void manageMsgCache();
};
