#pragma once

#include <string>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "Logger.h"
#include "EspGpioHelper.h"
#include "serial/SerialConnection.h"
#include "Utils.h"

#if defined (__cplusplus)
extern "C" {
#endif

#include "lora.h"

#if defined (__cplusplus)
}
#endif

#define LORA_FREQ_433E6 433E6
#define LORA_FREQ_866E6 866E6
#define LORA_FREQ_915E6 915E6
// Values from 0 to 500000
#define LORA_BANDWITH 400000
#define LORA_RECEIVE_BUFFER_SIZE 32
// Values from 2 to 17
#define LORA_TX_POWER 15
#define LORA_MIN_READ_BEFORE_SEND 100
// 100 milliseconds in microseconds
#define LORA_MIN_READ_TIME_MICRO 100 * 1000
// 100 milliseconds in microseconds
#define LORA_MIN_LAST_READ_DELAY_MICRO 100 * 1000
#define LORA_SEND_PROBABILITY 0.45

class AbstractLoraConnection {
 public:
    const uchar_sp WILDCARD_MAC = makeSharedUCharArrayPtr_del(new uchar[6]{0, 0, 0, 0, 0, 0}, 6);

    explicit AbstractLoraConnection(SerialConnection* serialC);
    virtual ~AbstractLoraConnection() = default;
    virtual void run() = 0;
    virtual void init();
    virtual void send(uchar_sp receiverMac, uchar_sp buf, int size) = 0;
    virtual void send(uchar_sp buf, int size) = 0;
    uchar_sp getChipMac();
    void printChipMac();
    static void printMac(uchar_sp mac);
    void setLocalMac(uchar_sp localMac);
    /**
     * Returns the localMac.
     * If the localMac is NULL it will default to the chipMac.
     */
    uchar_sp getLocalMac();

 protected:
    const std::string LOGGER_PREFIX = "[Lora] ";
    // The MAC address stored on the chip and loaded with loadChipMac():
    uchar_sp chipMac = NULL;
    uchar_sp localMac = NULL;
    SerialConnection* serialC = NULL;

    void loadChipMac();
    virtual void onLoraMessage(uchar_sp buf, int l) = 0;
    void receiveBurst();
    void loraSend(uchar_sp buf, int size);
    bool isTarget(uchar_sp localMac, uchar_sp receiverMac);
    bool compMac(uchar_sp a, uchar_sp b);
};
