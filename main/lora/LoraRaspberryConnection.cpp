#include "LoraRaspberryConnection.h"

LoraRaspberryConnection::LoraRaspberryConnection(SerialConnection* serialC) : AbstractLoraConnection(serialC) {
}

LoraRaspberryConnection::~LoraRaspberryConnection() {
    delete toSendAckQueue;
    delete toSendBurstQueue;
    delete burstNumberMutex;
    delete curBurstMutex;
}

void LoraRaspberryConnection::init() {
    if (state != lra_init) {
        log_error(LOGGER_PREFIX + "Failed to init. State != lra_init");
        return;
    }

    // Lora init:
    AbstractLoraConnection::init();

    // Fill send queue with test data:
    fillQueue();

    state = lra_ready;
    log_info(LOGGER_PREFIX + "Finished init.");
}

void LoraRaspberryConnection::send(uchar_sp buf, int size) {
    send(WILDCARD_MAC, buf, size);
}

void LoraRaspberryConnection::send(uchar_sp receiverMac, uchar_sp buf, int size) {
    std::shared_ptr<LoraMessageBurst> burst = std::make_shared<LoraMessageBurst>(LoraMessageBurst(getNextBurstNumber()));
    uchar flags = 0;
    uchar seqNumber = 0;
    uchar payloadLength = 0;
    std::shared_ptr<Message> msg;
    for (int i = 0; i < size; i += LORA_MAX_PAYLOAD_LENGTH) {
        if (burst->size() >= MAX_MSG_BURST_SIZE) {
            toSendBurstQueue->push(burst);
            burst = std::make_shared<LoraMessageBurst>(LoraMessageBurst(getNextBurstNumber()));
        }

        uchar_sp payload = NULL;
        if (i + LORA_MAX_PAYLOAD_LENGTH >= size) {
            payloadLength = size - i;
        } else {
            payloadLength = LORA_MAX_PAYLOAD_LENGTH;
        }

        payload = makeSharedUCharArrayPtr(payloadLength);
        cpyMsgPart(buf.get(), payload.get(), i, 0, payloadLength);

        flags = LORA_NO_FLAGS;
        if (i <= 0) {
            flags |= LORA_FIRST_MSG_FLAG;
        }

        if (i + LORA_MAX_PAYLOAD_LENGTH >= size -1) {
            flags |= LORA_LAST_MSG_FLAG;
        }

        seqNumber = burst->getNextSeqNumber();
        LoraDataMessage dataMsg = LoraDataMessage(flags, burst->getBurstNumber(), seqNumber, receiverMac, getLocalMac(), payloadLength, payload);
        msg = std::make_shared<Message>(Message());
        dataMsg.createBuffer(msg.get());
        burst->add(msg, seqNumber);
    }

    toSendBurstQueue->push(burst);

    if (toSendBurstQueue->size() > LORA_MAX_SEND_QUEUE) {
        serialC->sendBufFull();
    }
}

void LoraRaspberryConnection::run() {
    if (state != lra_ready) {
        log_error(LOGGER_PREFIX + "Failed to run. State != lra_ready");
        return;
    }

    while (1) {
        receiveBurst();
        manageMsgCache();

        // Send only with LORA_SEND_PROBABILITY probability:
        if (getRandomFloat() < LORA_SEND_PROBABILITY) {
            if (!sendOutstandingAckMessages()) {
                sendOutstandingDataMessages();
            }
        }
    }
}

uchar LoraRaspberryConnection::getNextBurstNumber() {
    std::unique_lock<std::mutex> mlock(*burstNumberMutex);
    // Only 4 bit are available for the msg sequence number:
    uchar c;
    if (nextBurstNumber >= 16) {
        nextBurstNumber = 0;
        c = 0;
    } else {
        c = nextBurstNumber++;
    }
    mlock.unlock();
    return c;
}

bool LoraRaspberryConnection::sendOutstandingAckMessages() {
    if (toSendAckQueue->size() > 0) {
        std::shared_ptr<Message> msg = NULL;
        for (int i = 0; i < 16 && toSendAckQueue->size() > 0; i++) {
            // Send and cleanup:
            msg = toSendAckQueue->pop();
            loraSend(msg->buf, msg->bufLength);
            log_debug(LOGGER_PREFIX + "Send ACK message.");
        }
        return true;
    }
    return false;
}

void LoraRaspberryConnection::sendOutstandingDataMessages() {
    if (state == lra_ready) {
        if (toSendBurstQueue->size() > 0) {
            std::shared_ptr<LoraMessageBurst> burst = toSendBurstQueue->pop();
            messageBurstSendAll(burst->getMap());
            burst->setResendCount(1);
            log_info(LOGGER_PREFIX + "Send burst: " + std::to_string(burst->getBurstNumber()));

            std::unique_lock<std::mutex> mlock(*curBurstMutex);
            curBurst = burst;
            mlock.unlock();

            state = lra_awaitAck;
            resetTimer();

            // Send buffer ready if space left:
            if (toSendBurstQueue->size() < LORA_MAX_SEND_QUEUE) {
                serialC->sendBufReady();
            }
        } else {
            fillQueue();
        }
    } else if (state == lra_awaitAck && hasAckTimerAlarmTriggered()) {
        std::unique_lock<std::mutex> mlock(*curBurstMutex);

        if (curBurst->getResendCount() > LORA_MAX_RESEND_COUNT) {
            if (curBurst->getBurstRetryCount() > LORA_MAX_BURST_RETRY_COUNT) {
                log_warn("Dropping burst " + std::to_string(curBurst->getBurstNumber()) + " - target does not answer!");
            } else {
                curBurst->setBurstRetryCount(curBurst->getBurstRetryCount() + 1);
                toSendBurstQueue->push(curBurst);
                log_info("Receiver for burst " + std::to_string(curBurst->getBurstNumber()) + " did not answer. Pushing back.");
                curBurst = NULL;
            }
            state = lra_ready;
        } else {
            curBurst->setResendCount(curBurst->getResendCount() + 1);
            messageBurstSendAll(curBurst->getMap());
            log_info(LOGGER_PREFIX + "Resend burst: " + std::to_string(curBurst->getBurstNumber()) +  " for the " + std::to_string(curBurst->getResendCount()) + " time");
            resetTimer();
        }
        mlock.unlock();
    }
}

void LoraRaspberryConnection::messageBurstSendAll(MsgBurstMap* map) {
    for (MsgBurstMap::iterator iter = map->begin(); iter != map->end(); iter++) {
        loraSend(iter->second->buf, iter->second->bufLength);
    }
}

void LoraRaspberryConnection::resetTimer() {
    nextAckTimerAlarm = esp_timer_get_time() + LORA_ACK_TIMER_DELAY_MICRO;
}

bool LoraRaspberryConnection::hasAckTimerAlarmTriggered() {
    return esp_timer_get_time() > nextAckTimerAlarm;
}

void LoraRaspberryConnection::fillQueue() {
    if (EspGpioHelper::getLoraRefillBufferGpioLevel()) {
        uchar_sp s = makeSharedUCharArrayPtr("{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}");
        send(s, 64);
        log_info(LOGGER_PREFIX + "Refilling queue.");
    }
}

void LoraRaspberryConnection::onLoraMessage(uchar_sp buf, int l) {
    uchar type = AbstractLoraMessage::getMsgTypeFromMessage(buf);
    uchar burstNumber = AbstractLoraMessage::getBurstNumberFromMessage(buf);
    uchar_sp senderMac = AbstractLoraMessage::getSenderMacFromMessage(buf);
    uchar_sp receiverMac = AbstractLoraMessage::getReceiverMacFromMessage(buf);

    if (!isTarget(getLocalMac(), receiverMac)) {
        log_info("Droping package of type " + std::to_string(type) + ". Receiver does not match:");
        printMac(receiverMac);
        return;
    } else {
        if (type == LORA_ACK_MSG_TYPE) {
            std::unique_lock<std::mutex> mlock(*curBurstMutex);
            if (!curBurst) {
                log_warn(LOGGER_PREFIX + "Received ACK but there is no curBurst.");
            } else {
                if (curBurst->getBurstNumber() == burstNumber) {
                    curBurst->onAckMessage(buf);

                    log_debug(LOGGER_PREFIX + "Received ACK for: " + std::to_string(burstNumber));
                    // All messages in burst ACKed?
                    if (curBurst->size() <= 0) {
                        EspGpioHelper::setLoraSendSuccessGpio(1);
                        log_info("Finished sending burst: " + std::to_string(curBurst->getBurstNumber()));
                        curBurst = NULL;
                        state = lra_ready;
                        EspGpioHelper::setLoraSendSuccessGpio(0);
                    }
                    nextAckTimerAlarm = 0;
                } else {
                    log_warn(std::to_string(curBurst->getBurstNumber()) + " != " + std::to_string(burstNumber));
                }
            }
            mlock.unlock();
        } else if (type == LORA_DATA_MSG_TYPE) {
            uchar seqNumber = LoraDataMessage::getSeqNumberFromMessage(buf);
            uchar payloadLength = LoraDataMessage::getPayloadLengthFromMessage(buf);
            uchar_sp payload = LoraDataMessage::getPayloadFromMessage(buf, payloadLength);

            uchar flags = LoraDataMessage::getFlagsFromMessage(buf);
            bool isFirstMsg = flags & LORA_FIRST_MSG_FLAG;
            bool isLastMsg = flags & LORA_LAST_MSG_FLAG;

            msgCache.onMessage(senderMac, payload, payloadLength, isFirstMsg, isLastMsg, burstNumber, seqNumber);
            log_debug("Received Lora data message burst " + std::to_string(burstNumber) + ", seqNumber: " + std::to_string(seqNumber));
        } else {
            log_warn(LOGGER_PREFIX + "Received Lora message with unknown type: " + std::to_string(type));
        }
    }
}

void LoraRaspberryConnection::manageMsgCache() {
    CheckResult result;
    msgCache.checkAll(&result);

    // Send received messages over serial:
    for (std::string s : result.messages) {
        EspGpioHelper::setLoraReceiveSuccessGpio(1);
        log_warn("Lora received: " + s);
        serialC->sendLoraReceived(s);
        EspGpioHelper::setLoraReceiveSuccessGpio(1);
    }

    // Send ACKs for all changed bursts:
    LoraAckMessage* ackMsg = NULL;
    for (ReceivedMsgBurst_sp burst : result.changed) {
        if (burst->allPartsReceived) {
            ackMsg = new LoraAckMessage(LORA_ALL_RECEIVED_FLAG, burst->burstNumber, 0, burst->senderMac, getLocalMac(), NULL);

        } else {
            size_t ackCount = 0;
            uchar_sp ackNumbers = msgCache.getToAckParts(burst, &ackCount);
            ackMsg = new LoraAckMessage(LORA_NO_FLAGS, burst->burstNumber, ackCount, burst->senderMac, getLocalMac(), ackNumbers);
        }

        std::shared_ptr<Message> msg = std::make_shared<Message>(Message());
        ackMsg->createBuffer(msg.get());
        toSendAckQueue->push(msg);
        delete ackMsg;
    }
}
