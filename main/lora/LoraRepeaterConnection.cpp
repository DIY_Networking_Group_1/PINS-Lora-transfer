#include "LoraRepeaterConnection.h"

LoraRepeaterConnection::LoraRepeaterConnection(SerialConnection* serialC) : AbstractLoraConnection(serialC) {}

LoraRepeaterConnection::~LoraRepeaterConnection() {
    delete repeatQueue;
}

void LoraRepeaterConnection::init() {
    if (state != lre_init) {
        log_error(LOGGER_PREFIX + "Failed to init. State != lre_init");
        return;
    }

    // General Lora init:
    AbstractLoraConnection::init();

    state = lre_ready;
}

void LoraRepeaterConnection::run() {
    if (state != lre_ready) {
        log_error(LOGGER_PREFIX + "Failed to run. State != lre_ready");
        return;
    }

    while (1) {
        receiveBurst();

        // Send only with LORA_SEND_PROBABILITY probability:
        if (getRandomFloat() < LORA_SEND_PROBABILITY) {
            sendRepeatMessages();
        }
    }
}

void LoraRepeaterConnection::onLoraMessage(uchar_sp buf, int l) {
    RepeatMessage* msg = new RepeatMessage();
    msg->buf = buf;
    msg->bufLength = l;
    repeatQueue->push(msg);
}

void LoraRepeaterConnection::sendRepeatMessages() {
    RepeatMessage* msg = NULL;
    for (int i = 0; i < 16 && repeatQueue->size() > 0; i++) {
        msg = repeatQueue->pop();
        loraSend(msg->buf, msg->bufLength);
        delete msg;
    }
}

void LoraRepeaterConnection::send(uchar_sp receiverMac, uchar_sp buf, int size) {
    log_warn("LoraRepeaterConnection::send(uchar_sp receiverMac, uchar_sp buf, int size) is not used!");
}

void LoraRepeaterConnection::send(uchar_sp buf, int size) {
    log_warn("LoraRepeaterConnection::send(uchar_sp receiverMac, uchar_sp buf) is not used!");
}
