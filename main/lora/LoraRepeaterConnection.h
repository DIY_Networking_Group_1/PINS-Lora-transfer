#pragma once

#include <atomic>
#include "AbstractLoraConnection.h"
#include "Queue.h"
#include "Utils.h"

struct RepeatMessage {
    uchar_sp buf;
    int bufLength;
};

enum LoraRepeaterConnectionState {
    lre_init,
    lre_ready,
};

class LoraRepeaterConnection : public AbstractLoraConnection {
 public:
    explicit LoraRepeaterConnection(SerialConnection* serialC);
    ~LoraRepeaterConnection();
    void init() override;
    void run() override;
    void send(uchar_sp receiverMac, uchar_sp buf, int size) override;
    void send(uchar_sp buf, int size) override;

 private:
    std::atomic<LoraRepeaterConnectionState> state {lre_init};
    Queue<RepeatMessage*>* repeatQueue = new Queue<RepeatMessage*>();

    void onLoraMessage(uchar_sp buf, int l) override;
    void sendRepeatMessages();
};
