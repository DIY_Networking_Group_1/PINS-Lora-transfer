#pragma once

#include <cstddef>
#include "AbstractLoraMessage.h"

#define LORA_ACK_MSG_TYPE 0b10
#define LORA_MASK_ACK_COUNT 0b00001111

#define LORA_ALL_RECEIVED_FLAG 0b00000001

#define LORA_MAX_ACK_NUMBERS_COUNT 16

class LoraAckMessage : public AbstractLoraMessage {
 public:
    LoraAckMessage(uchar flags, uchar burstNumber, uchar ackCount, uchar_sp receiverMac, uchar_sp senderMac, uchar_sp ackNumbers);
    ~LoraAckMessage() {}
    void createBuffer(Message* msg) override;

    uchar getAckCount();
    uchar_sp getAckNumbers();

    static uchar getAckCountFromMessage(uchar_sp buffer);
    static uchar_sp getAckNumbersFromMessage(uchar_sp buffer, uchar ackCount);

 protected:
    uchar ackCount;
    uchar_sp ackNumbers;
};
