#pragma once

#include <cstddef>
#include "AbstractLoraMessage.h"
#include "messages/MessageTools.h"

#define LORA_MAX_PAYLOAD_LENGTH 17
#define LORA_DATA_MSG_TYPE 0b01

#define LORA_FIRST_MSG_FLAG 0b00000001
#define LORA_LAST_MSG_FLAG 0b00000010

#define LORA_MASK_SEQ_NUMBER 0b00001111

class LoraDataMessage : public AbstractLoraMessage {
 public:
    LoraDataMessage(uchar flags, uchar burstNumber, uchar seqNumber, uchar_sp receiverMac, uchar_sp senderMac, uchar payloadLength, uchar_sp payload);
    ~LoraDataMessage() {}
    void createBuffer(Message* msg) override;
    uchar getSeqNumber();
    uchar getPayloadLength();
    uchar_sp getPayload();
    static uchar getSeqNumberFromMessage(uchar_sp buffer);
    static uchar getPayloadLengthFromMessage(uchar_sp buffer);
    static uchar_sp getPayloadFromMessage(uchar_sp buffer, uchar payloadLength);

 protected:
    uchar seqNumber;
    uchar payloadLength;
    uchar_sp payload;
};
