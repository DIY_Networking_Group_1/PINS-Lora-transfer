#pragma once

#include "messages/MessageTools.h"
#include "Utils.h"

#define LORA_MASK_MSG_TYPE 0b11000000
#define LORA_MASK_BURST_NUMBER 0b11110000

#define LORA_MASK_FLAGS 0b00111111

#define LORA_NO_FLAGS 0b00000000

struct Message {
    uchar_sp buf;
    unsigned int bufLength;
};

class AbstractLoraMessage {
 public:
    explicit AbstractLoraMessage(uchar msgType, uchar flags, uchar burstNumber, uchar_sp receiverMac, uchar_sp senderMac);
    AbstractLoraMessage() = default;
    virtual ~AbstractLoraMessage() {}
    virtual void createBuffer(Message* msg) { msg->bufLength = 0; }
    void createBaseBuffer(Message* msg);

    uchar getFlags();
    static uchar getMsgTypeFromMessage(uchar_sp buffer);
    static uchar getBurstNumberFromMessage(uchar_sp buffer);
    static uchar getFlagsFromMessage(uchar_sp buffer);
    static uchar_sp getReceiverMacFromMessage(uchar_sp buffer);
    static uchar_sp getSenderMacFromMessage(uchar_sp buffer);
    uchar getMsgType();
    uchar getBurstNumber();
    uchar_sp getSenderMac();
    uchar_sp getReceiverMac();

 protected:
    uchar msgType;
    uchar flags;
    uchar burstNumber;
    uchar_sp senderMac;
    uchar_sp receiverMac;
};
