#include "LoraDataMessage.h"

LoraDataMessage::LoraDataMessage(uchar flags, uchar burstNumber, uchar seqNumber, uchar_sp receiverMac, uchar_sp senderMac, uchar payloadLength, uchar_sp payload) : AbstractLoraMessage(LORA_DATA_MSG_TYPE, flags, burstNumber, receiverMac, senderMac) {
    this->seqNumber = seqNumber;
    this->payloadLength = payloadLength;
    this->payload = payload;
}

void LoraDataMessage::createBuffer(Message* msg) {
    msg->buf = makeSharedUCharArrayPtr(32);
    msg->bufLength = 32;

    createBaseBuffer(msg);
    msg->buf.get()[1] |= seqNumber & LORA_MASK_SEQ_NUMBER;
    msg->buf.get()[14] = payloadLength;
    if (payload != NULL) {
        cpyMsgPart(payload.get(), msg->buf.get(), 0, 15, LORA_MAX_PAYLOAD_LENGTH);
    }
}

uchar LoraDataMessage::getSeqNumber() {
    return seqNumber;
}

uchar LoraDataMessage::getPayloadLength() {
    return payloadLength;
}

uchar_sp LoraDataMessage::getPayload() {
    return payload;
}

uchar LoraDataMessage::getSeqNumberFromMessage(uchar_sp buffer) {
    return (buffer.get()[1] & LORA_MASK_SEQ_NUMBER);
}

uchar LoraDataMessage::getPayloadLengthFromMessage(uchar_sp buffer) {
    return buffer.get()[14];
}

uchar_sp LoraDataMessage::getPayloadFromMessage(uchar_sp buffer, uchar payloadLength) {
    uchar_sp payload = makeSharedUCharArrayPtr(payloadLength);
    getBytesWithOffset(buffer.get(), 15 * 8, 8 * payloadLength, payload.get());
    return payload;
}
