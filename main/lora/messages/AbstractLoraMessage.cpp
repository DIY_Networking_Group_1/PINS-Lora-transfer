#include "AbstractLoraMessage.h"

AbstractLoraMessage::AbstractLoraMessage(uchar msgType, uchar flags, uchar burstNumber, uchar_sp receiverMac, uchar_sp senderMac) {
    this->msgType = msgType;
    this->flags = flags;
    this->burstNumber = burstNumber;
    this->receiverMac = receiverMac;
    this->senderMac = senderMac;
}

uchar AbstractLoraMessage::getMsgType() {
    return msgType;
}

uchar AbstractLoraMessage::getFlags() {
    return flags;
}

uchar AbstractLoraMessage::getBurstNumber() {
    return burstNumber;
}

uchar_sp AbstractLoraMessage::getReceiverMac() {
    return receiverMac;
}

uchar_sp AbstractLoraMessage::getSenderMac() {
    return senderMac;
}

uchar AbstractLoraMessage::getMsgTypeFromMessage(uchar_sp buffer) {
    return (buffer.get()[0] & LORA_MASK_MSG_TYPE) >> 6;
}

uchar AbstractLoraMessage::getFlagsFromMessage(uchar_sp buffer) {
    return buffer.get()[0] & LORA_MASK_FLAGS;
}

uchar_sp AbstractLoraMessage::getReceiverMacFromMessage(uchar_sp buffer) {
    uchar_sp macOut = makeSharedUCharArrayPtr(6);
    getBytesWithOffset(buffer.get(), 2 * 8, 8 * 6, macOut.get());
    return macOut;
}

uchar_sp AbstractLoraMessage::getSenderMacFromMessage(uchar_sp buffer) {
    uchar_sp macOut = makeSharedUCharArrayPtr(6);
    getBytesWithOffset(buffer.get(), 8 * 8, 8 * 6, macOut.get());
    return macOut;
}

uchar AbstractLoraMessage::getBurstNumberFromMessage(uchar_sp buffer) {
    return (buffer.get()[1] & LORA_MASK_BURST_NUMBER) >> 4;
}

void AbstractLoraMessage::createBaseBuffer(Message *msg) {
    msg->buf.get()[0] = ((msgType << 6) & LORA_MASK_MSG_TYPE) | (flags & LORA_MASK_FLAGS);
    msg->buf.get()[1] = (burstNumber << 4) & LORA_MASK_BURST_NUMBER;
    cpyMsgPart(receiverMac.get(), msg->buf.get(), 0, 2, 6);
    cpyMsgPart(senderMac.get(), msg->buf.get(), 0, 8, 6);
}
