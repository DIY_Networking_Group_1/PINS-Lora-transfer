#include "LoraAckMessage.h"

LoraAckMessage::LoraAckMessage(uchar flags, uchar burstNumber, uchar ackCount, uchar_sp receiverMac, uchar_sp senderMac, uchar_sp ackNumbers) : AbstractLoraMessage(LORA_ACK_MSG_TYPE, flags, burstNumber, receiverMac, senderMac) {
    this->ackCount = ackCount;
    this->ackNumbers = ackNumbers;
}

void LoraAckMessage::createBuffer(Message* msg) {
    msg->buf = makeSharedUCharArrayPtr(14 + ackCount);
    msg->bufLength = 14 + ackCount;

    createBaseBuffer(msg);
    msg->buf.get()[1] |= ackCount & LORA_MASK_ACK_COUNT;
    if (ackNumbers != NULL) {
        cpyMsgPart(ackNumbers.get(), msg->buf.get(), 0, 14, ackCount);
    }
}

uchar LoraAckMessage::getAckCount() {
    return ackCount;
}

uchar_sp LoraAckMessage::getAckNumbers() {
    return ackNumbers;
}

uchar LoraAckMessage::getAckCountFromMessage(uchar_sp buffer) {
    return buffer.get()[1] & LORA_MASK_ACK_COUNT;
}

uchar_sp LoraAckMessage::getAckNumbersFromMessage(uchar_sp buffer, uchar ackCount) {
    uchar_sp ackNumbers = makeSharedUCharArrayPtr(ackCount);
    cpyMsgPart(buffer.get(), ackNumbers.get(), 14, 0, ackCount);
    return ackNumbers;
}
