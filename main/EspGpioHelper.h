#pragma once

#include "driver/gpio.h"

#define LORA_SEND_GPIO GPIO_NUM_25
#define LORA_SEND_SUCCESS_GPIO GPIO_NUM_12
#define LORA_RECEIVE_SUCCESS_GPIO GPIO_NUM_17
#define LORA_REFILL_BUFFER_GPIO GPIO_NUM_36
#define LORA_MODE_OUT_GPIO GPIO_NUM_21
#define LORA_MODE_IN_GPIO GPIO_NUM_13

class EspGpioHelper {
 public:
    static void init();
    static void setLoraSendGpio(uint32_t level);
    static void setLoraSendSuccessGpio(uint32_t level);
    static void setLoraReceiveSuccessGpio(uint32_t level);
    static void setLoraModeOutGpio(uint32_t level);
    static bool getLoraRefillBufferGpioLevel();
    static bool getLoraModeInGpioLevel();
};
