#include "EspGpioHelper.h"

void EspGpioHelper::init() {
    gpio_pad_select_gpio(LORA_SEND_GPIO);
    gpio_set_direction(LORA_SEND_GPIO, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LORA_SEND_SUCCESS_GPIO);
    gpio_set_direction(LORA_SEND_SUCCESS_GPIO, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LORA_RECEIVE_SUCCESS_GPIO);
    gpio_set_direction(LORA_RECEIVE_SUCCESS_GPIO, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LORA_REFILL_BUFFER_GPIO);
    gpio_set_direction(LORA_REFILL_BUFFER_GPIO, GPIO_MODE_INPUT);

    gpio_pad_select_gpio(LORA_MODE_OUT_GPIO);
    gpio_set_direction(LORA_MODE_OUT_GPIO, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LORA_MODE_IN_GPIO);
    gpio_set_direction(LORA_MODE_IN_GPIO, GPIO_MODE_INPUT);
}

void EspGpioHelper::setLoraSendGpio(uint32_t level) {
    gpio_set_level(LORA_SEND_GPIO, level);
}

void EspGpioHelper::setLoraSendSuccessGpio(uint32_t level) {
    gpio_set_level(LORA_SEND_SUCCESS_GPIO, level);
}

void EspGpioHelper::setLoraReceiveSuccessGpio(uint32_t level) {
    gpio_set_level(LORA_RECEIVE_SUCCESS_GPIO, level);
}

void EspGpioHelper::setLoraModeOutGpio(uint32_t level) {
    gpio_set_level(LORA_MODE_OUT_GPIO, level);
}

bool EspGpioHelper::getLoraRefillBufferGpioLevel() {
    return gpio_get_level(LORA_REFILL_BUFFER_GPIO);
}

bool EspGpioHelper::getLoraModeInGpioLevel() {
    return gpio_get_level(LORA_MODE_IN_GPIO);
}
