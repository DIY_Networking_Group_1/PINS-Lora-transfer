PROJECT_NAME=PINS-Lora-Transfer
BUILD_DIR=build

include $(IDF_PATH)/make/project.mk

default:
	make clean2
	make all

init:
	git submodule init
	git submodule update

test:
	make default
	
clean2:
	if [ -d $(BUILD_DIR) ]; then rm -rf $(BUILD_DIR); fi