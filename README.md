# PINS-Lora-Transfer

## Description:
PINS-Lora-Transfer is the ESP32 software written in C/C++ which allows the data transfer between two [Heltec Wifi LoRa 32](https://www.hackerspace-ffm.de/wiki/index.php?title=Heltec_Wifi_LoRa_32) devices.
It get's data it has to send from the via USB (UART) connect [PINS Base Station](https://gitlab.com/DIY_Networking_Group_1/PINS-BM). Also it forwards all received data back to the [PINS Base Station](https://gitlab.com/DIY_Networking_Group_1/PINS-BM) via the USB (UART) connection.
Some of it's features are:
* Error correction for the transferred data
* Antenna multiplexing so only one antenna is used for TX and RX
* It archives a absolute data rate of about 70 Bytes per second under optimal conditions
* We were able to successfully transfer data over 530 meters with buildings in between
* Without buildings in between the possible distance should be much higher (max < 10 km)
* For the transfer a [custom protocol](https://gitlab.com/DIY_Networking_Group_1/PINS-Proctocoll/wikis/Lora-Protocol) is used

## The Heltec Wifi LoRa 32:
During the time I was working with the [Heltec Wifi LoRa 32](https://www.hackerspace-ffm.de/wiki/index.php?title=Heltec_Wifi_LoRa_32) microcontroller I've noticed the following things:

### `make monitor` outputs only jibberish:
If you flash the Heltec ESP and then start the ESP-IDF monitor with `make monitor` it will only show jibberish. This is caused by ESP-IDF being configured by default to work with chips that have a 40MHz crystal. So either:
* Change the baudrate with that you are reading to `74880`
* Or run `make menuconfig` and change the crystal frequency from 40MHz to 26MHz via "Component config" ---> "ESP32-specific" ---> "Main XTAL frequency (40 MHz)"

### Adding additional folders to organize source code:
To tell `ESP-IDF` to include source code files in additional folders you've created during the build process, edit the `component.mk` to something like [this](https://gitlab.com/DIY_Networking_Group_1/PINS-Lora-transfer/blob/master/main/component.mk).

## Dependencies:
* [esp-idf v3.1](https://github.com/espressif/esp-idf/releases/tag/v3.1)
* [esp32-lora-library](https://github.com/Inteform/esp32-lora-library)
* gcc >7.0

## Installation:
Once [esp-idf](https://github.com/espressif/esp-idf) and [esp32-lora-library](https://github.com/Inteform/esp32-lora-library) are installed, run the following command to compile the project:
```
make compile
```

To flash the ESP32 run:
```
make flash
```

To build only the app and keep the already build framework run:
```
make app
```

To monitor the serial output run:
```
make monitor
```